﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIPolygon : MonoBehaviour {

    public Polygon polygon;
    void OnGUI()
    {
        var count = 0;
        var units = Object.FindObjectsOfType<Unit>();
        for (int i = 0; i < units.Length; i++)
        {
            if (polygon.InPolygon(units[i].transform.position))
                count++;
        }


        GUI.Label(new Rect(10, 30, 100, 100), count.ToString());
    }
}
