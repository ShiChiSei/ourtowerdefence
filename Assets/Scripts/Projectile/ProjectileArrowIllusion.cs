﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileArrowIllusion : Projectile
{
    public float delay;
    public float duration;

    protected override void CollisionWithTarget()
    {
        target.TakeAttack(Effect.Get(EffectId.Illusion, damage, duration, delay));
        DestroyProjectile();
    }
}