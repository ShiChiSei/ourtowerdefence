﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileArrowParalysis : Projectile
{

    public float delay;
    public float duration;
    public float interval;
    public int count;

    protected override void CollisionWithTarget()
    {
        for (int i = 0; i < count; i++)
        {
            target.TakeAttack(Effect.Get(EffectId.Paralysis, damage, duration, delay + (interval + duration) * i));
        }
        DestroyProjectile();
    }
}
