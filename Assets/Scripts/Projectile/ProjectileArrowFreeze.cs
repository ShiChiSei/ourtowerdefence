﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileArrowFreeze : Projectile
{
    public float delay;
    public float duration;

    protected override void CollisionWithTarget()
    {
        target.TakeAttack(Effect.Get(EffectId.Freeze, damage, duration, delay));
        DestroyProjectile();
    }
}
