﻿using UnityEngine;
using System.Collections;


public abstract class Projectile : MonoBehaviour
{
	/// the speed of the projectile
	protected float speed;
	protected int damage;

	/// the projectile's initial velocity
	public GameObject DestroyedFX;
	public AudioClip ExplosionSFX;

	protected Unit target;


	public void Initialize(Unit target, int damage, float speed)
	{
		this.target = target;
		this.damage = damage;
		this.speed = speed;
	}

	public void LateUpdate()
	{
		if (target == null)
		{
			DestroyProjectile();
			return;
		}

		CheckForCollision();
		Movement();
	}


	void CheckForCollision()
	{
		var distToTarget = target.transform.position - transform.position;

		if (distToTarget.sqrMagnitude < 0.05f)
		{
			CollisionWithTarget();
		}
	}

	protected abstract void CollisionWithTarget();

	void Movement()
	{

        var lerpQ = Mathf.Clamp(speed * Time.deltaTime / Vector3.Distance(transform.position, target.transform.position), 0, 1);
        var LerpVector = Vector3.Lerp(transform.position, target.transform.position, lerpQ);

        transform.LookAt(LerpVector, Vector3.up);
		transform.position = LerpVector;
	}

	protected void DestroyProjectile()
	{
		if (DestroyedFX != null)
		{
			Instantiate(DestroyedFX, transform.position, Quaternion.identity);
		}

		Destroy(gameObject);
	}
}