﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileArrowFire : Projectile
{

    public float delay;
    public float duration;

    protected override void CollisionWithTarget()
    {
        target.TakeAttack(Effect.Get(EffectId.Fire, damage, duration, delay));
        DestroyProjectile();
    }
}
