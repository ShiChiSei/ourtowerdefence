﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFactory : MonoBehaviour {

	public static Projectile InstantiateProjectile(Projectile projectilePrefab, Vector3 originPos, Unit target, int damage, float speed){
		Projectile p = Instantiate (projectilePrefab, originPos, Quaternion.identity);
		p.Initialize (target, damage, speed);
		return p;
	}
}
