﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSplash : Projectile
{
    public float DamageRange;

    Unit[] GetEnemysByType()
    {
        var enemies = Object.FindObjectsOfType<Unit>();
        List<Unit> units = new List<Unit>();
        for (int i = 0; i < enemies.Length; i++)
        {
            Unit unit = enemies[i];
            if (unit.Properties.Health == 0) continue;

            float dist = Vector3.Distance(transform.position, unit.transform.position);
            if (dist < DamageRange)
            {
                units.Add(unit);
            }
        }
        return units.ToArray();
    }

    protected override void CollisionWithTarget()
    {
        foreach (var unit in GetEnemysByType())
        {
            unit.TakeAttack(Effect.Get(EffectId.Damage, damage));
        }
        DestroyProjectile();
    }
}
