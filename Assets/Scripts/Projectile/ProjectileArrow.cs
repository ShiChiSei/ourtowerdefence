﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileArrow : Projectile
{
    protected override void CollisionWithTarget()
    {
        target.TakeAttack(Effect.Get(EffectId.Damage, damage));
        DestroyProjectile();
    }
}
