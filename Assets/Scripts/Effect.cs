﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : System.ICloneable, System.IEquatable<Effect>
{


    public EffectId Id { get; private set; }
    public String Name { get; private set; }
    public int Priority { get; private set; }
    public bool IsIgnoresDefence { get; private set; }
    public EffectDamageType Type;

    private Effect() { }
    public static Effect Get(EffectId id, float damage = 0, float duration = 0, float delay = 0)
    {
        Effect effect = new Effect();
        effect.Id = id;
        switch (id)
        {
            case EffectId.Damage:
                if (damage < 0) damage = 0;
                effect.Duration = duration;
                effect.Delay = delay;
                if (duration == 0 && delay == 0)
                {
                    effect.Type = EffectDamageType.Instant;
                }
                else if (duration == 0)
                {
                    effect.Type = EffectDamageType.Deferred;
                }
                else
                {
                    effect.Type = EffectDamageType.Recurrent;
                }
                effect.Damage = damage;
                return effect;

            case EffectId.HealthRecovery:
                if (damage < 0) damage = 0;
                effect.Type = EffectDamageType.Recurrent;
                effect.Damage = -damage;
                effect.Duration = duration;
                effect.Delay = delay;
                effect.IsIgnoresDefence = true;
                return effect;

            case EffectId.Fire:
                if (damage < 0) damage = 0;
                effect.Type = EffectDamageType.Recurrent;
                effect.Damage = damage;
                effect.Duration = duration;
                effect.Delay = delay;
                effect.SpeedQ = 2f;
                return effect;

            case EffectId.Paralysis:
                if (damage < 0) damage = 0;
                effect.Type = EffectDamageType.Deferred;
                effect.Damage = damage;
                effect.Duration = duration;
                effect.Delay = delay;
                effect.SpeedQ = 0f;
                return effect;

            case EffectId.Freeze:
                if (damage < 0) damage = 0;
                effect.Type = EffectDamageType.Recurrent;
                effect.Damage = damage;
                effect.Duration = duration;
                effect.Delay = delay;
                effect.SpeedQ = 0.5f;
                return effect;

            case EffectId.Illusion:
                if (damage < 0) damage = 0;
                effect.Type = EffectDamageType.Recurrent;
                effect.Damage = damage;
                effect.Duration = duration;
                effect.Delay = delay;
                effect.DefenceQ = -0.5f;
               effect.SpeedQ = 1.25f;
                return effect;

        }
        return effect;
    }

    public static float GetSpeedQ(IEnumerable<Effect> effects)
    {
        float speedQ = 1;
        List<int> temp = new List<int>();
        foreach (var effect in effects)
        {
            if (effect.IsEnabled)
            {
                if (!temp.Contains((int)effect.Id))
                {
                    speedQ *= effect.SpeedQ;
                    temp.Add((int)effect.Id);
                }
            }
        }
        return speedQ;
    }

    public static float GetDefenceQ(IEnumerable<Effect> effects)
    {
        float defenceQ = 1;
        List<int> temp = new List<int>();
        foreach (var effect in effects)
        {
            if (effect.IsEnabled)
            {
                if (!temp.Contains((int)effect.Id))
                {
                    defenceQ *= effect.DefenceQ;
                    temp.Add((int)effect.Id);
                }
            }
        }
        return defenceQ;
    }


    public float Damage
    {
        get
        {
            return damage;
        }

        set
        {
            damage = value;
            temp_damageGradual = value;
            temp_damageRecurrent = value;

        }
    }
    private float damage;
   
    /// <summary>
    /// Задержка
    /// </summary>
    public float Delay
    {
        get
        {
            return delay;
        }

        set
        {
            delay = value;
            temp_delay = value;
        }
    }
    private float delay;
    private float temp_delay;

    /// <summary>
    /// Продолжительность эффекта
    /// </summary>
    public float Duration
    {
        get
        {
            return duration;
        }

        set
        {
            duration = value;
            temp_duration = value;
            temp_durationGradual = value;
            temp_durationRecurrent = value;
            temp_timeRecurrent = value;
        }
    }
    private float duration;
    private float temp_duration;


    private float deltaTime;
    public float DeltaTime
    {
        get
        {
            return deltaTime;
        }
    }

    public bool SetDeltaTime(float deltaTime)
    {
        IsInitialize = true;

        if (this.temp_delay > 0)
        {
            float temp = this.temp_delay - deltaTime;
            this.temp_delay = Mathf.Clamp(temp, 0, this.delay);
            if (temp < 0)
            {
                this.temp_duration = Mathf.Clamp(this.temp_duration + temp, 0, this.duration);
            }
        }
        else if (temp_duration > 0)
        {
            this.temp_duration = Mathf.Clamp(this.temp_duration - deltaTime, 0, this.duration);
        }
        else
        {
            this.deltaTime = 0;
            return false;
        }
        this.deltaTime = deltaTime;
        return true;
    }

    public void TakeDamage(ICanTakeDamage takeDamage, float defence)
    {
        float damageQ = 1;
        if (!IsIgnoresDefence)
        {
            if (defence < 0)
            {
                var temp_defence = defence;
                if (temp_defence < -20)
                {
                    temp_defence = -20;
                }
                damageQ = 1 - Mathf.Pow(0.94f, -temp_defence);
            }
            else
            {
                damageQ = (0.06f * defence) / (1 + 0.06f * defence);
            }
            damageQ = (1 - damageQ);
        }

        IsInitialize = true;

        if (Type == EffectDamageType.Instant)
        {
            takeDamage.TakeDamage(damage * damageQ);
            Type = EffectDamageType.None;
        }

        if (Type == EffectDamageType.Deferred && IsEnabled)
        {
            takeDamage.TakeDamage(damage * damageQ);
            Type = EffectDamageType.None;
        }

        if (Type == EffectDamageType.Gradual && (temp_durationGradual > temp_duration || (temp_durationGradual - temp_duration != 0 && !IsEnabled)))
        {
            float temp_damageQ = (temp_durationGradual - temp_duration) / duration;

            float damage = this.damage * temp_damageQ;

            takeDamage.TakeDamage(damage * damageQ);

            temp_damageGradual -= damage;

            temp_durationGradual = temp_duration;

            if (!IsEnabled)
            {
                takeDamage.TakeDamage(temp_damageGradual * damageQ);
                Type = EffectDamageType.None;
            }
        }

        if (Type == EffectDamageType.Recurrent && (((temp_durationRecurrent - temp_duration) > (1 / damageRecurrentPeriud)) || (temp_durationRecurrent - temp_duration != 0 && !IsEnabled)))
        {
            if (isDamageRecurrentPeriudFromSec)
            {
                temp_timeRecurrent -= 1 / damageRecurrentPeriud;

                float temp_damageQ = 1 / damageRecurrentPeriud;
                if (temp_timeRecurrent < 0)
                {
                    temp_damageQ *= -temp_timeRecurrent;
                }

                takeDamage.TakeDamage(this.damage * temp_damageQ * damageQ);
                temp_durationRecurrent = temp_duration;
                if (!IsEnabled)
                {
                    Type = EffectDamageType.None;
                }
            }
            else
            {
                float temp_damageQ = 1 / (damageRecurrentPeriud * duration);

                float damage = this.damage * temp_damageQ;

                takeDamage.TakeDamage(damage * damageQ);

                temp_damageRecurrent -= damage;

                temp_durationRecurrent = temp_duration;

                if (!IsEnabled)
                {
                    takeDamage.TakeDamage(temp_damageRecurrent * damageQ);
                    Type = EffectDamageType.None;
                }
            }
        }
    }

    /// <summary>
    /// Постепенный урон
    /// Происходит потеря точности, нужно исправить
    /// </summary>
    private float temp_durationGradual;
    private float temp_damageGradual;

    /// <summary>
    /// Периодический урон
    /// Происходит потеря точности, нужно исправить
    /// </summary>
    private float temp_durationRecurrent;
    private float temp_damageRecurrent;
    private float temp_timeRecurrent;

    /// <summary>
    /// Количество урона в секунду
    /// </summary>
    public float DamageRecurrentPeriud
    {
        get
        {
            return damageRecurrentPeriud;
        }

        set
        {
            damageRecurrentPeriud = value;
        }
    }
    private float damageRecurrentPeriud = 2;
    private bool isDamageRecurrentPeriudFromSec = true;




    /// <summary>
    /// Модификатор скорости
    /// </summary>
    public float SpeedQ = 1;
    /// <summary>
    /// Модификатор защиты
    /// </summary>
    public float DefenceQ = 1;
    /// <summary>
    /// Модификатор здоровья
    /// </summary>
    public float HealthQ = 1;




    public bool IsEnabled
    {
        get
        {
            return temp_delay <= 0 && temp_duration > 0;
        }
    }

    public bool IsInitialize { get; private set; }

    public object Clone()
    {
        Effect effect = new Effect()
        {
            Priority = this.Priority,
            Id = this.Id,
            Name = this.Name,

            Delay = this.Delay,
            Duration = this.Duration,

            SpeedQ = this.SpeedQ,
            DefenceQ = this.DefenceQ,
            HealthQ = this.HealthQ
        };
        return effect;
    }

    public bool Equals(Effect other)
    {
        return this.Id == other.Id;
    }
}
