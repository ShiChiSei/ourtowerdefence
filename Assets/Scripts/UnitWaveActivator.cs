﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitWaveActivator : MonoBehaviour {


    public BezierSpline spline;

    public UnitWave[] Waves;

    public float interval;

    private int index;
    

    // Use this for initialization
    void Start () {

        foreach (var wave in Waves)
        {
            wave.spline = spline;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (Waves.Length != 0 && index < Waves.Length)
        {
            if (Waves[index].time > 0)
            {
                Waves[index].time -= Time.deltaTime;
            }
            else
            {
                float time_Offset = 0;
                foreach (var unit in Waves[index].Units)
                {
                    if (unit == null) continue;

                    StartCoroutine(UnityTimer.Start(time_Offset, (System.Action)(() => {
                        Instantiate(unit, (Vector3)spline.Calculations.GetPoint(0), Quaternion.identity);
                    })));

                    time_Offset += interval;

                }
                index ++;
            }
        }
	}
}
