﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitWave : MonoBehaviour {

    public BezierSpline spline;

    public Unit[] Units;

    public float time;

    // Use this for initialization
    void Start () {
        foreach (var unit in Units)
        {
            if (unit == null)
                continue;

            unit.Spline = spline;
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
