﻿

public enum EffectId
{
    None,
    Damage,
    Fire,
    Paralysis,
    Freeze,
    Illusion,
    HealthRecovery
}
