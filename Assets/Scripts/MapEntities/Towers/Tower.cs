﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour 
{
    //[UnityEngine.Header("Состаяния")]
    //[UnityEngine.HideInInspector]
    public bool IsActive = true;

    [UnityEngine.Header("Свойства")]
    public TowerProperties Properties;

    //[UnityEngine.Header("Целb для атаки")]
    //[UnityEngine.SerializeField]
    [UnityEngine.HideInInspector]

    public Unit[] AttackTargets;
    [UnityEngine.HideInInspector]
    protected float timeOfLastAttack;
    [UnityEngine.HideInInspector]
    protected float timeBetweenAttacks;

    protected virtual void InitializeProperties()
    {
        UpdateAttackSpeed();
    }

    protected virtual void UpdateAttackSpeed()
    {
        timeBetweenAttacks = 1 / Properties.AttacksPerSecond;
    }

    protected virtual void Start()
    {
        InitializeProperties();
        DetermineTarget();
    }

    protected virtual void Attack()
    {
        ProjectileFactory.InstantiateProjectile(Properties.ProjectilePrefab, transform.position, AttackTargets[0], Properties.Damage, Properties.ProjectileSpeed);
        timeOfLastAttack = Time.time;
    }

    protected virtual void Update () 
	{
        if (!IsActive) return;

		DetermineTarget();

		//Атаковать цель
		if (AttackTargets[0] != null) {
			if (Time.time - timeOfLastAttack > timeBetweenAttacks) {
				Attack();
			}
		}
	}

    /// <summary>
    /// Определить цель
    /// </summary>
	protected virtual void DetermineTarget()
	{
        if (AttackTargets == null)
        {
            AttackTargets = new Unit[1];
        }
        if (AttackTargets.Length != 1)
        {
            AttackTargets = new Unit[1];
        }

        //1. Если цели нет, то выбираем ближайшую
        if (AttackTargets[0] == null)
        {
            GetClosestEnemyByType();
            return;
        }

		if (AttackTargets[0].Properties.Health == 0) {
			AttackTargets[0] = null;
			return;
		}

		//2. Если текущая цель вне радиуса пушки - выбираем ближайшую
		if(Vector3.Distance(transform.position, AttackTargets[0].transform.position) > Properties.TowerRange){
			GetClosestEnemyByType();
		}

		//3. Если условия не соблюдены - оставляем текущую цель
	}

    /// <summary>
    /// Получить ближайшего юнита
    /// </summary>
	protected virtual void GetClosestEnemyByType()
	{
        var enemies = Object.FindObjectsOfType<Unit> ();

		AttackTargets[0] = null;
		float closestDistance = 1000f;

		for (int i = 0; i < enemies.Length; i++) {
			Unit unit = enemies [i];
			if (unit.Properties.Health == 0) continue;

			float dist = Vector3.Distance (transform.position, unit.transform.position);
			if (dist > Properties.TowerRange) continue;

			if (dist < closestDistance || unit.Properties.Priority > AttackTargets[0].Properties.Priority) {
				closestDistance = dist;
				AttackTargets[0] = unit;
			}
		}
	}
}
