﻿using System;

[Serializable]
public struct TowerProperties
{
    public Projectile ProjectilePrefab;

    public float TowerRange;
    public int Damage;


    public float ProjectileSpeed;
    public float AttacksPerSecond;

    public float Price;



}


