﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayTower : Tower {

    protected override void Attack()
    {
        for (int i = 0; i < AttackTargets.Length; i++)
        {
            AttackTargets[i].TakeAttack(Effect.Get(EffectId.Damage, Properties.Damage));
        }

        timeOfLastAttack = Time.time;
    }

    protected override void Update()
    {
        if (!IsActive) return;

        DetermineTarget();

        //Атаковать цель
        if (AttackTargets.Length != 0)
        {
            if (Time.time - timeOfLastAttack > timeBetweenAttacks)
            {
                Attack();
            }
        }
    }

    /// <summary>
    /// Определить цель
    /// </summary>
	protected override void DetermineTarget()
    {
        AttackTargets = new Unit[0];

        GetClosestEnemyByType();
    }

    /// <summary>
    /// Получить ближайшего юнита
    /// </summary>
	protected override void GetClosestEnemyByType()
    {
        var enemies = UnityEngine.Object.FindObjectsOfType<Unit>();

        for (int i = 0; i < enemies.Length; i++)
        {
            Unit unit = enemies[i];
            if (unit.Properties.Health == 0) continue;

            float dist = Vector3.Distance(transform.position, unit.transform.position);

            if (dist > Properties.TowerRange) continue;

            Array.Resize(ref AttackTargets, AttackTargets.Length + 1);
            AttackTargets[AttackTargets.Length - 1] = unit;
        }
    }
}
