﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(Tower))]
public class TowerInspector : Editor {

    private Tower tower;
    private Transform handleTransform;
    private Quaternion handleRotation;
    private int selectedIndex = -1;

    private const float handleSize = 0.04f;
    private const float pickSize = 0.06f;

    private void OnSceneGUI()
    {
        tower = target as Tower;

        if (!tower.IsActive) return;

        handleTransform = this.tower.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ?
            handleTransform.rotation : Quaternion.identity;

        Handles.color = Color.yellow;

        Handles.Disc(Quaternion.identity, tower.transform.position, new Vector3(0, 0, 1), this.tower.Properties.TowerRange, true, 1);

        Vector3 point = handleTransform.position + new Vector3(0, this.tower.Properties.TowerRange,0);
        float size = HandleUtility.GetHandleSize(point) * 2;
        if (Handles.Button(point, handleRotation, size * handleSize, size * pickSize, Handles.DotHandleCap))
        {
            selectedIndex = 0;
            Repaint();
        }
        if (selectedIndex == 0)
        {
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, handleRotation);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(tower, "Move Point");

                var distance = Vector3.Distance(tower.transform.position, point);
                if (distance >= 0)
                    this.tower.Properties.TowerRange = distance;

                EditorUtility.SetDirty(tower);
            }
        }

    }
}
