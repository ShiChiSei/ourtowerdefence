﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct UnitProperties
{
    public float MaxHealth;
    public float Health;

    public float Speed;
    public float Attack;

    public float Defence;

    public int Priority;
    public float Price;

    public List<Effect> Effects;

    public Vector3 Offset;
    public float Progress;

    public float DodgeChance;// шанс уклонения

    public bool SlowImmunity;// имунитет к замедлению
    public bool BashImmunity;// имунитет к оглушению
}

