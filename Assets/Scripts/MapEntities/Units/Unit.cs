﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Unit : MonoBehaviour, ICanTakeDamage
{
    //[UnityEngine.Header("Состаяния")]
    //[UnityEngine.HideInInspector]
    public bool IsDie = false;
    //[UnityEngine.HideInInspector]
    public bool IsMove = true;
    //[UnityEngine.HideInInspector]
    public bool IsAttack = false;

    [UnityEngine.Header("Траектория")]
    public BezierSpline Spline;
    private BezierSplineСalculations SplineСalculations;

    [UnityEngine.Header("Свойства")]
    public UnitProperties Properties;

    protected Animator animator;

    protected virtual void InitializeProperties()
    {
        Properties.Health = Properties.MaxHealth;

        Properties.Effects = new List<Effect>();

        Properties.Offset = Random.insideUnitSphere * 0.7f;
        Properties.Offset.z = 0;

        SplineСalculations = Spline.Calculations.Copy(Properties.Offset.x);
    }

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();

        InitializeProperties();
    }

    protected virtual void Update()
    {
        if (IsDie) return;

        ApplyEffects(); //Применяем эффекты

        if (IsMove) SplineMovement(); // Двигать юнит по сплайну

        if (IsSpecialAbility()) SpecialAbility();

        UpdateAnimationController();
    }


    protected virtual void SpecialAbility()
    {

    }

    protected virtual bool IsSpecialAbility()
    {
        return false;
    }

    protected void ApplyEffects()
    {
        Effect[] temp_effects = Properties.Effects.ToArray();
        foreach (var effect in temp_effects)
        {

            bool isRemoved = !effect.SetDeltaTime(Time.deltaTime);

            effect.TakeDamage(this, Properties.Defence * Effect.GetDefenceQ(Properties.Effects));

            if (isRemoved)
            {
                Properties.Effects.Remove(effect);
            }
        }
    }

    public virtual void TakeAttack(Effect effect)
    {
        if (Random.value < Properties.DodgeChance && effect.Id != EffectId.HealthRecovery)
        {
            PopUpText("мимо", 1f, Color.blue);
            return;
        }
        if ((effect.Id == EffectId.Paralysis && Properties.BashImmunity) || (effect.Id == EffectId.Freeze && Properties.SlowImmunity))
        {
            effect = Effect.Get(EffectId.Damage, effect.Damage, effect.Duration, effect.Delay);
        }
        Properties.Effects.Add(effect);
    }


    protected void SplineMovement()
    {
        float speed = Properties.Speed * Effect.GetSpeedQ(Properties.Effects);

        Properties.Progress += speed * Time.deltaTime;
        if (Properties.Progress > SplineСalculations.length)
            Properties.Progress -= SplineСalculations.length;

        this.transform.position = SplineСalculations.GetPoint(Properties.Progress);
    }


    protected void UpdateAnimationController()
    {
        animator.SetFloat("Speed", Properties.Speed * Effect.GetSpeedQ(Properties.Effects));
    }


    public virtual void TakeDamage(float damage)
    {
        takeDamage(damage);
    }
    protected void takeDamage(float damage)
    {

        var temp_damage = -damage;
        if (temp_damage > 0)
        {
            PopUpText("+" + Mathf.Round(temp_damage).ToString(), 1f, Color.green);
        }
        else
        {
            PopUpText(Mathf.Round(temp_damage).ToString(), 1f, Color.red);
        }

        Properties.Health = Mathf.Clamp(Properties.Health - damage, 0, Properties.MaxHealth);
        if (Properties.Health == 0)
        {
            Die();
        }
    }


    protected void Die()
    {
        if (IsDie) return;
        else IsDie = true;

        Player.Instanse.Price += Properties.Price;

        Properties.Speed = 0f;

        animator.SetBool("isAttacking", false);
        animator.SetBool("isDying", true);

        StartCoroutine(UnityTimer.Start(2f, () =>
        {
            Destroy(gameObject);
        })
        );
    }


    [UnityEngine.Header("GUI")]
    public Texture GUIHealthBackground;
    public Texture GUIHealthForeground;
    public float GUIHealthHeightOffset = -40;
    public float GUIHealthHeight = 4;
    public float GUIHealthWidth = 30;
    
    private List<float> GUIPopUpTextStartTimes = new List<float>();
    private List<float> GUIPopUpTextDurations = new List<float>();
    private List<string> GUIPopUpTexts = new List<string>();
    private List<Color> GUIPopUpColors = new List<Color>();

    private void PopUpText(string t, float d, Color c)
    {
        GUIPopUpTextStartTimes.Add(Time.time);
        GUIPopUpTextDurations.Add(d);
        GUIPopUpTexts.Add(t);
        GUIPopUpColors.Add(c);
    }

    protected virtual void OnGUI()
    {
        if (IsDie) return;

        Vector2 position = Camera.main.WorldToScreenPoint(transform.position);

        GUI.DrawTexture(new Rect(position.x - GUIHealthWidth / 2, -position.y + Screen.height + GUIHealthHeightOffset, GUIHealthWidth, GUIHealthHeight), GUIHealthBackground);
        GUI.DrawTexture(new Rect(position.x - GUIHealthWidth / 2, -position.y + Screen.height + GUIHealthHeightOffset, Properties.Health / Properties.MaxHealth * GUIHealthWidth, GUIHealthHeight), GUIHealthForeground);

        GUIStyle style = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 7 };

        GUI.Label(new Rect(position.x - GUIHealthWidth, -position.y + Screen.height + GUIHealthHeightOffset * 1.75f, GUIHealthWidth * 2, GUIHealthHeight * 10), Mathf.Round(Properties.Health) + "/" + Mathf.Round(Properties.MaxHealth), style);


        style = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 7 };        GUI.color = Color.red;        for (int i = 0; i < GUIPopUpTexts.Count; )
        {
            var temp = Time.time - GUIPopUpTextStartTimes[i];
            if (temp > GUIPopUpTextDurations[i])
            {
                GUIPopUpTextStartTimes.RemoveAt(i);
                GUIPopUpTextDurations.RemoveAt(i);
                GUIPopUpTexts.RemoveAt(i);
                GUIPopUpColors.RemoveAt(i);
            }
            GUI.color = GUIPopUpColors[i]; 

            GUI.Label(new Rect(position.x - GUIHealthWidth + temp / GUIPopUpTextDurations[i] * 20, -position.y + Screen.height + GUIHealthHeightOffset * 1.75f - temp / GUIPopUpTextDurations[i] * 20, GUIHealthWidth * 2, GUIHealthHeight * 10), GUIPopUpTexts[i], style);

            i++;
        }
    }
}
