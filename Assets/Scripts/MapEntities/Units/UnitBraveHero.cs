﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBraveHero : Unit
{
    protected override void SpecialAbility()
    {
        var towers = Object.FindObjectsOfType<Tower>();

        for (int i = 0; i < towers.Length; i++)
        {
            Tower tower = towers[i];

            float dist = Vector3.Distance(transform.position, tower.transform.position);

            if (dist < tower.Properties.TowerRange)
            {
                tower.AttackTargets = new Unit[1];
                tower.AttackTargets[0] = this;
            }
        }
    }

    protected override void Start()
    {
        specialAbilityTimer = specialAbilityTime;

        base.Start();
    }

    protected override void Update()
    {
        specialAbilityTimer -= Time.deltaTime;

        base.Update();
    }

    private float specialAbilityTime = 5;
    private float specialAbilityTimer;

    protected override bool IsSpecialAbility()
    {
        if (specialAbilityTimer <= 0)
        {
            specialAbilityTimer = specialAbilityTime;
            return true;
        }
        return false;
    }
}
