﻿using UnityEngine;

public class UnitMage : Unit
{
    [Header("Свойства Mage")]
    public float Range;
    public float DamageHealth;

    protected override void SpecialAbility()
    {
        var enemies = Object.FindObjectsOfType<Unit>();
        for (int i = 0; i < enemies.Length; i++)
        {
            Unit unit = enemies[i];
            if (unit == this) continue;
            if (unit.Properties.Health == 0) continue;

            float dist = Vector3.Distance(transform.position, unit.transform.position);
            if (dist < Range)
            {
                Effect effect = Effect.Get(EffectId.HealthRecovery, DamageHealth, 1);
                if (!unit.Properties.Effects.Contains(effect))
                {
                    unit.TakeAttack(effect);
                }
            }
        }
    }

    protected override void Start()
    {
        specialAbilityTimer = specialAbilityTime;

        base.Start();
    }

    protected override void Update()
    {
        specialAbilityTimer -= Time.deltaTime;

        base.Update();
    }

    private float specialAbilityTime = 5;
    private float specialAbilityTimer;

    protected override bool IsSpecialAbility()
    {
        if(specialAbilityTimer <= 0)
        {
            specialAbilityTimer = specialAbilityTime;
            return true;
        }
        return false;
    }
}
