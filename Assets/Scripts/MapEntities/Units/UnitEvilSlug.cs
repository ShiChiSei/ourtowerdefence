﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitEvilSlug : Unit {

    [UnityEngine.Header("Увеличение здоровья от урона")]
    public float Q = 0.66f;
    public override void TakeDamage(float damage)
    {
        Properties.MaxHealth += Q * damage;
        Properties.Health += Q * damage;

        takeDamage(damage);
    }
}
