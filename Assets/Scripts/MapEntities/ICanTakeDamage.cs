﻿using System;


public interface ICanTakeDamage
{
    void TakeDamage(float damage);
}


