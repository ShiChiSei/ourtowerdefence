﻿using System;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// http://rsdn.org/article/multimedia/Bezier.xml
/// </summary>
public class AdaptivePartitioningBezierCurve
{
    const float curve_collinearity_epsilon = 1e-30f;
    const float curve_angle_tolerance_epsilon = 0.00f;
    const int curve_recursion_limit = 32;

    private float distance_tolerance_square;
    private float angle_tolerance;
    private float cusp_limit;

    public List<Vector3> Points = new List<Vector3>();



    /// <summary>
    /// cusp_limit — Угол в радианах. Если 0, то только реальные точки перегиба будут выглядеть как “bevel join”. Значения больше нуля ограничивают “остроту” на поворотах. Чем больше это значение, тем менее крутые повороты будут “обрезаны”. Обычно это значение не должно превышать 10-15 градусов (в “радиановом” эквиваленте).
    /// </summary>
    public float CuspLimit
    {
        get
        {
            return (cusp_limit == 0.0) ? 0.0f : Mathf.PI - cusp_limit;
        }

        set
        {
            cusp_limit = (value == 0.0) ? 0.0f : Mathf.PI - value;
        }
    }
    /// <summary>
    /// angle_tolerance — Задается в радианах. Чем меньше это значение, тем более точно будет аппроксимирована кривая на крутых поворотах. Однако 0 означает, что угловые условия вообще не учитываются.
    /// </summary>
    public float AngleTolerance
    {
        get
        {
            return angle_tolerance;
        }

        set
        {
            angle_tolerance = value;
        }
    }

    public AdaptivePartitioningBezierCurve(BezierCurve b) : this(b.P0, b.P1, b.P2, b.P3) { }

    public AdaptivePartitioningBezierCurve(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        angle_tolerance = 15f / 180f * Mathf.PI;
        cusp_limit = 15f / 180f * Mathf.PI;
        distance_tolerance_square = 0.01f;
        distance_tolerance_square *= distance_tolerance_square;
        Initialize(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
    }
    private void Reset()
    {
        Points.Clear();
    }
    private void Initialize(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
    {
        Reset();
        Bezier(x1, y1, x2, y2, x3, y3, x4, y4);
    }
    private float Distance(float x0, float y0, float x1, float y1)
    {
        return Mathf.Sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
    }
    private void RecursiveBezier(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, int level)
    {
        if (level > curve_recursion_limit)
        {
            return;
        }

        // Calculate all the mid-points of the line segments
        //----------------------
        float x12 = (x1 + x2) / 2;
        float y12 = (y1 + y2) / 2;
        float x23 = (x2 + x3) / 2;
        float y23 = (y2 + y3) / 2;
        float x34 = (x3 + x4) / 2;
        float y34 = (y3 + y4) / 2;
        float x123 = (x12 + x23) / 2;
        float y123 = (y12 + y23) / 2;
        float x234 = (x23 + x34) / 2;
        float y234 = (y23 + y34) / 2;
        float x1234 = (x123 + x234) / 2;
        float y1234 = (y123 + y234) / 2;


        // Try to approximate the full cubic curve by a single straight line
        //------------------
        float dx = x4 - x1;
        float dy = y4 - y1;

        float d2 = Mathf.Abs(((x2 - x4) * dy - (y2 - y4) * dx));
        float d3 = Mathf.Abs(((x3 - x4) * dy - (y3 - y4) * dx));
        float da1, da2, k;

        switch (((int)(d2 > curve_collinearity_epsilon ? 1 : 0) << 1) + (int)(d3 > curve_collinearity_epsilon ? 1 : 0))
        {
            case 0:
                // All collinear OR p1==p4
                //----------------------
                k = dx * dx + dy * dy;
                if (k == 0)
                {
                    d2 = Distance(x1, y1, x2, y2);
                    d3 = Distance(x4, y4, x3, y3);
                }
                else
                {
                    k = 1 / k;
                    da1 = x2 - x1;
                    da2 = y2 - y1;
                    d2 = k * (da1 * dx + da2 * dy);
                    da1 = x3 - x1;
                    da2 = y3 - y1;
                    d3 = k * (da1 * dx + da2 * dy);
                    if (d2 > 0 && d2 < 1 && d3 > 0 && d3 < 1)
                    {
                        // Simple collinear case, 1---2---3---4
                        // We can leave just two endpoints
                        return;
                    }
                    if (d2 <= 0) d2 = Distance(x2, y2, x1, y1);
                    else if (d2 >= 1) d2 = Distance(x2, y2, x4, y4);
                    else d2 = Distance(x2, y2, x1 + d2 * dx, y1 + d2 * dy);

                    if (d3 <= 0) d3 = Distance(x3, y3, x1, y1);
                    else if (d3 >= 1) d3 = Distance(x3, y3, x4, y4);
                    else d3 = Distance(x3, y3, x1 + d3 * dx, y1 + d3 * dy);
                }
                if (d2 > d3)
                {
                    if (d2 < distance_tolerance_square)
                    {
                        Points.Add(new Vector3(x2, y2));
                        return;
                    }
                }
                else
                {
                    if (d3 < distance_tolerance_square)
                    {
                        Points.Add(new Vector3(x3, y3));
                        return;
                    }
                }
                break;

            case 1:
                // p1,p2,p4 are collinear, p3 is significant
                //----------------------
                if (d3 * d3 <= distance_tolerance_square * (dx * dx + dy * dy))
                {
                    if (angle_tolerance < curve_angle_tolerance_epsilon)
                    {
                        Points.Add(new Vector3(x23, y23));
                        return;
                    }

                    // Angle Condition
                    //----------------------
                    da1 = Mathf.Abs(Mathf.Atan2(y4 - y3, x4 - x3) - Mathf.Atan2(y3 - y2, x3 - x2));
                    if (da1 >= Mathf.PI) da1 = 2 * Mathf.PI - da1;

                    if (da1 < angle_tolerance)
                    {
                        Points.Add(new Vector3(x2, y2));
                        Points.Add(new Vector3(x3, y3));
                        return;
                    }

                    if (cusp_limit != 0.0)
                    {
                        if (da1 > cusp_limit)
                        {
                            Points.Add(new Vector3(x3, y3));
                            return;
                        }
                    }
                }
                break;

            case 2:
                // p1,p3,p4 are collinear, p2 is significant
                //----------------------
                if (d2 * d2 <= distance_tolerance_square * (dx * dx + dy * dy))
                {
                    if (angle_tolerance < curve_angle_tolerance_epsilon)
                    {
                        Points.Add(new Vector3(x23, y23));
                        return;
                    }

                    // Angle Condition
                    //----------------------
                    da1 = Mathf.Abs(Mathf.Atan2(y3 - y2, x3 - x2) - Mathf.Atan2(y2 - y1, x2 - x1));
                    if (da1 >= Mathf.PI) da1 = 2 * Mathf.PI - da1;

                    if (da1 < angle_tolerance)
                    {
                        Points.Add(new Vector3(x2, y2));
                        Points.Add(new Vector3(x3, y3));
                        return;
                    }

                    if (cusp_limit != 0.0)
                    {
                        if (da1 > cusp_limit)
                        {
                            Points.Add(new Vector3(x2, y2));
                            return;
                        }
                    }
                }
                break;

            case 3:
                // Regular case
                //-----------------
                if ((d2 + d3) * (d2 + d3) <= distance_tolerance_square * (dx * dx + dy * dy))
                {
                    // If the curvature doesn't exceed the distance_tolerance value
                    // we tend to finish subdivisions.
                    //----------------------
                    if (angle_tolerance < curve_angle_tolerance_epsilon)
                    {
                        Points.Add(new Vector3(x23, y23));
                        return;
                    }

                    // Angle & Cusp Condition
                    //----------------------
                    k = Mathf.Atan2(y3 - y2, x3 - x2);
                    da1 = Mathf.Abs(k - Mathf.Atan2(y2 - y1, x2 - x1));
                    da2 = Mathf.Abs(Mathf.Atan2(y4 - y3, x4 - x3) - k);
                    if (da1 >= Mathf.PI) da1 = 2 * Mathf.PI - da1;
                    if (da2 >= Mathf.PI) da2 = 2 * Mathf.PI - da2;

                    if (da1 + da2 < angle_tolerance)
                    {
                        // Finally we can stop the recursion
                        //----------------------
                        Points.Add(new Vector3(x23, y23));
                        return;
                    }

                    if (cusp_limit != 0.0)
                    {
                        if (da1 > cusp_limit)
                        {
                            Points.Add(new Vector3(x2, y2));
                            return;
                        }

                        if (da2 > cusp_limit)
                        {
                            Points.Add(new Vector3(x3, y3));
                            return;
                        }
                    }
                }
                break;
        }

        // Continue subdivision
        //----------------------
        RecursiveBezier(x1, y1, x12, y12, x123, y123, x1234, y1234, level + 1);
        RecursiveBezier(x1234, y1234, x234, y234, x34, y34, x4, y4, level + 1);
    }
    private void Bezier(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
    {
        Points.Add(new Vector3(x1, y1));
        RecursiveBezier(x1, y1, x2, y2, x3, y3, x4, y4, 0);
        Points.Add(new Vector3(x4, y4));
    }
    public void AddBezier(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
    {
        if (Points[Points.Count - 1] != new Vector3(x1, y1))
        {

            Debug.LogError("Кривая не является частью сплайна");
            return;
        }
        RecursiveBezier(x1, y1, x2, y2, x3, y3, x4, y4, 0);
        Points.Add(new Vector3(x4, y4));
    }

    public void AddBezier(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        AddBezier(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
    }

}