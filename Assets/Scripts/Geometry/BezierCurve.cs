﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurve
{
    public Vector3 P0;
    public Vector3 P1;
    public Vector3 P2;
    public Vector3 P3;
    public BezierCurve(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        this.P0 = p0;
        this.P1 = p1;
        this.P2 = p2;
        this.P3 = p3;
    }
    public void Reset()
    {
        P0 = new Vector3(1f, 0f, 0f);
        P1 = new Vector3(2f, 0f, 0f);
        P2 = new Vector3(3f, 0f, 0f);
        P3 = new Vector3(4f, 0f, 0f);
    }
    public Vector3 GetPoint(float t)
    {
        return GetPoint(P0, P1, P2, P3, t);
    }
    public Vector3 GetVelocity(float t)
    {
        return GetFirstDerivative(P0, P1, P2, P3, t);
    }
    public Vector3 GetDirection(float t)
    {
        return GetVelocity(t).normalized;
    }
    public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = t > 1 ? 1 : t < 0 ? 0 : t;
        float oneMinusT = 1f - t;
        return Mathf.Pow(oneMinusT, 3) * p0 + 3f * Mathf.Pow(oneMinusT, 2) * t * p1 + 3f * oneMinusT * Mathf.Pow(t, 2) * p2 + Mathf.Pow(t, 3) * p3;
    }
    public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = t > 1 ? 1 : t < 0 ? 0 : t;
        float oneMinusT = 1f - t;
        return 3f * Mathf.Pow(oneMinusT, 2) * (p1 - p0) + 6f * oneMinusT * t * (p2 - p1) + 3f * Mathf.Pow(t, 2) * (p3 - p2);
    }

}
