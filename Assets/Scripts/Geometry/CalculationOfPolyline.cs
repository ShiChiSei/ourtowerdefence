﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class CalculationOfPolyline
{

    public Vector3[] Points { get; private set; }
    public float Length { get; private set; }
    public bool Loop { get; private set; }
    private float[] Distances;

    public CalculationOfPolyline(Vector3[] points)
    {
        if (points.Length == 0)
        {
            throw new Exception();
        }

        if (points.Length == 1)
        {
            Points = points;
            return;
        }

        List<Vector3> tempPoints = new List<Vector3>();
        List<float> tempDistances = new List<float>();
        float distance = 0;
        int i = 0;
        int k = 0;

        Length = 0;

        for (; i < points.Length - 1; i++)
        {
            distance = Vector3.Distance(points[i], points[i + 1]);

            if (distance != 0)
            {
                tempDistances.Add(distance);
                tempPoints.Add(points[i]);
                Length += tempDistances[i - k];

                if (i != 0)
                {
                    tempDistances[i - k] += tempDistances[i - k - 1];
                }
            }
            else
            {
                k++;
            }
        }
        tempPoints.Add(points[i]);

        Points = tempPoints.ToArray();
        Distances = tempDistances.ToArray();
    }

    public Vector3 GetPoint(float progress)
    {
        if (Points.Length == 0)
            throw new Exception();

        if (Points.Length == 1)
            return Points[0];

        if (Loop)
        {
            while (progress < 0)
            {
                progress += Length;
            }
            while (progress > Length)
            {
                progress -= Length;
            }
        }

        if (progress <= 0)
        {
            return Points[0];
        }
        if (progress >= Length)
        {
            return Points[Points.Length - 1];
        }

        if (progress < Distances[0])
        {
            return Vector3.Lerp(Points[0], Points[1], progress / Distances[0]);
        }

        for (int i = 1; i < Distances.Length;)
        {
            if (progress > Distances[i])
            {
                i++;
            }
            else
            {
                float t = (progress - Distances[i - 1]) / (Distances[i] - Distances[i - 1]);
                return Vector3.Lerp(Points[i], Points[i + 1], t);
            }
        }

        throw new Exception();
    }


}
