﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectDamageType {
    /// <summary>
    /// Нет урона
    /// </summary>
    None,
    /// <summary>
    /// Мгновенный урон
    /// </summary>
    Instant,
    /// <summary>
    /// Отложенный урон
    /// </summary>
    Deferred,
    /// <summary>
    /// Постепенный урон
    /// Происходит потеря точности, нужно исправить
    /// </summary>
    Gradual,
    /// <summary>
    /// Периодический урон
    /// Происходит потеря точности, нужно исправить
    /// </summary>
    Recurrent
}
