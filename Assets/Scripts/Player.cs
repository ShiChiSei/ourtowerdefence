﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {

    private static Player obj;

    public static Player Instanse
    {
        get
        {
            if(obj == null)
            {
                obj = new Player();
            }
            return obj;
        }
    }

    private Player()
    {
        Price = 100;
    }

    public float Price;
}
