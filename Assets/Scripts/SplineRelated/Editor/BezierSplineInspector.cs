﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(BezierSpline))]
public class BezierSplineInspector : Editor
{
    private BezierSpline spline;
    private Transform handleTransform;
    private Quaternion handleRotation;
    private int selectedIndex = -1;

    private const float handleSize = 0.04f;
    private const float pickSize = 0.06f;

    private const int stepsPerCurve = 25;
    //private const float directionScale = 0.5f;

    private static Color[] modeColors = {
        Color.white,
        Color.yellow,
        Color.cyan
    };

    public override bool RequiresConstantRepaint()
    {
        spline.Calculations.Calculate();
        return base.RequiresConstantRepaint();
    }

    public override void OnInspectorGUI()
    {
        spline = target as BezierSpline;
        EditorGUI.BeginChangeCheck();
        bool loop = EditorGUILayout.Toggle("Loop", spline.Loop);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Toggle Loop");
            EditorUtility.SetDirty(spline);
            spline.Loop = loop;
        }
        if (selectedIndex >= 0 && selectedIndex < spline.ControlPointCount)
        {
            DrawSelectedPointInspector();
        }
        if (GUILayout.Button("Add Curve"))
        {
            Undo.RecordObject(spline, "Add Curve");
            spline.AddCurve();

            EditorUtility.SetDirty(spline);
        }
        if (GUILayout.Button("Remove Curve"))
        {
            Undo.RecordObject(spline, "Remove Curve");
            spline.RemoveCurve();

            EditorUtility.SetDirty(spline);
        }
    }

    private void DrawSelectedPointInspector()
    {
        EditorGUILayout.Separator();
        GUILayout.Label("Selected Point");
        EditorGUI.BeginChangeCheck();
        Vector3 point = EditorGUILayout.Vector3Field("Position", spline.GetControlPoint(selectedIndex));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Move Point");

            spline.SetControlPoint(selectedIndex, point);

            EditorUtility.SetDirty(spline);
        }
        EditorGUI.BeginChangeCheck();
        BezierControlPointMode mode = (BezierControlPointMode)EditorGUILayout.EnumPopup("Mode", spline.GetControlPointMode(selectedIndex));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Change Point Mode");

            spline.SetControlPointMode(selectedIndex, mode);

            EditorUtility.SetDirty(spline);
        }
        if (selectedIndex > 3)
        {
            if (GUILayout.Button("Remove Point"))
            {
                Undo.RecordObject(spline, "Remove Point");

                while (spline.ControlPointCount > selectedIndex)
                {
                    spline.RemoveCurve();
                }

                EditorUtility.SetDirty(spline);
            }
        }
        EditorGUILayout.Separator();
    }

    private void OnSceneGUI()
    {
        spline = target as BezierSpline;
        handleTransform = spline.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ?
            handleTransform.rotation : Quaternion.identity;

        ShowPoints();

        ShowLines();

        ShowDirections();

        Test(0.7f);
    }

    private void ShowPoints()
    {
        Vector3 p0 = ShowPoint(0);
        for (int i = 1; i < spline.ControlPointCount; i += 3)
        {
            Vector3 p1 = ShowPoint(i);
            Vector3 p2 = ShowPoint(i + 1);
            Vector3 p3 = ShowPoint(i + 2);

            Handles.color = Color.gray;
            Handles.DrawLine(p0, p1);
            Handles.DrawLine(p2, p3);

            Handles.DrawBezier(p0, p3, p1, p2, Color.white, null, 2f);
            p0 = p3;
        }
    }

    private void ShowLines()
    {
        Handles.color = Color.green;

        for (int i = 1; i < spline.Calculations.points.Length; i++)
        {
            Handles.DrawLine(spline.Calculations.points[i - 1], spline.Calculations.points[i]);
        }
    }

    private void Test(float d)
    {
        BezierSplineСalculations array = spline.Calculations.Copy(d);
        Handles.color = Color.red;

        for (int i = 1; i < array.points.Length; i++)
        {
            Handles.DrawLine(array.points[i - 1], array.points[i]);
        }

        array = spline.Calculations.Copy(-d);
        Handles.color = Color.blue;

        for (int i = 1; i < array.points.Length; i++)
        {
            Handles.DrawLine(array.points[i - 1], array.points[i]);
        }
    }

    private void ShowDirections(float height = 0)
    {
        for (int i = 0; i <= stepsPerCurve; i++)
        {
            Vector3 point = spline.Calculations.GetPoint((float)((i / (double)stepsPerCurve) * spline.Calculations.length));

            Handles.color = Color.green;
            Handles.DrawLine(point, point + (height * Vector3.up));
        }
    }

    private Vector3 ShowPoint(int index)
    {
        Vector3 point = handleTransform.TransformPoint(spline.GetControlPoint(index));
        float size = HandleUtility.GetHandleSize(point);
        if (index == 0)
        {
            size *= 2f;
        }
        Handles.color = modeColors[(int)spline.GetControlPointMode(index)];

        if (Handles.Button(point, handleRotation, size * handleSize, size * pickSize, Handles.DotHandleCap))
        {
            selectedIndex = index;
            Repaint();
        }

        if (selectedIndex == index)
        {
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, handleRotation);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Move Point");

                spline.SetControlPoint(index, handleTransform.InverseTransformPoint(point));

                EditorUtility.SetDirty(spline);
            }
        }
        return point;
    }
}