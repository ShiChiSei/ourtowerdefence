﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierSplineСalculations {

    private BezierSpline spline;
    public BezierSpline Spline
    {
        get
        {
            return spline;
        }

        set
        {
            spline = value;
            Calculate();
        }
    }

    public CalculationOfPolyline calculationOfPolyline { get; private set; }

    public BezierSplineСalculations(BezierSpline spline)
    {
        this.spline = spline;
        Calculate();
    }

    private BezierSplineСalculations(Vector3[] points)
    {
        calculationOfPolyline = new CalculationOfPolyline(points);
    }

    public void Calculate()
    {
        if (Spline == null)
        {
            Debug.LogError("spline == null");
            return;
        }

        AdaptivePartitioningBezierCurve apbc = null;

        for (int i = 0; i < Spline.CurveCount; i++)
        {
            Vector3 p0 = Spline.GetControlPoint((3 * i));
            Vector3 p1 = Spline.GetControlPoint((3 * i) + 1);
            Vector3 p2 = Spline.GetControlPoint((3 * i) + 2);
            Vector3 p3 = Spline.GetControlPoint((3 * i) + 3);
            if (apbc == null)
            {
                apbc = new AdaptivePartitioningBezierCurve(p0, p1, p2, p3);
            }
            else
            {
                apbc.AddBezier(p0, p1, p2, p3);
            }
        }

        calculationOfPolyline = new CalculationOfPolyline(apbc.Points.ToArray());

    }

    public Vector3[] points
    {
        get
        {
            return calculationOfPolyline.Points;
        }
    }

    public float length
    {
        get
        {
            return calculationOfPolyline.Length;
        }
    }

    public Vector3 GetPoint(float value)
    {
       return calculationOfPolyline.GetPoint(value);
    }

    public BezierSplineСalculations Copy(float d = 0)
    {
        return new BezierSplineСalculations(GetPointsParallelFromBisector(points, d));
    }






    private static Vector3 rot(Vector3 point)
    {
        return new Vector3(-point.y, point.x, point.z);
    }

    public static Vector3[] GetPointsParallelFromBisector(Vector3[] points, float d)
    {
        List<Vector3> result = new List<Vector3>();

        float sign = Mathf.Sign(d);

        for (int i = 0; i < points.Length; i++)
        {
            if (i == 0)
            {
                Vector3 a = points[i];
                Vector3 b = points[i + 1];
                Vector3 na = rot((b - a).normalized) * sign;
                if (sign > 0)
                {
                    na *= -1;
                }
                Vector3 newa = a + (na * d);
                result.Add(newa);


            }
            else if (i == points.Length - 1)
            {

                Vector3 a = points[i - 1];
                Vector3 b = points[i];
                Vector3 nb = rot((a - b).normalized);
                Vector3 newb = b + (nb * d);
                result.Add(newb);
            }
            else
            {
                Vector3 a = points[i - 1];
                Vector3 b = points[i];
                Vector3 c = points[i + 1];

                Vector3 na = rot((b - a).normalized) * -sign;
                Vector3 nc = rot((b - c).normalized) * sign;

                Vector3 newa = b + (na * d);
                Vector3 newc = b + (nc * d);

                Vector3 newb = new Vector3((newa.x + newc.x) / 2, (newa.y + newc.y) / 2);
                result.Add(((newb - b).normalized * d) + b);
            }
        }
        return result.ToArray();
    }
}
