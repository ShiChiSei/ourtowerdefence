﻿using UnityEngine;

public static class Bezier
{


    public static float[] CalcLookUpTable(ref float totalLength, int n, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float[] arr = new float[n];

        arr[0] = 0f;


        totalLength = 0f;//float totalLength = 0f;

        Vector3 prev = p0;

        for (int i = 1; i < arr.Length; i++)
        {
            float t = i / (float)(arr.Length - 1);
            Vector3 pt = GetPoint(p0, p1, p2, p3, t);
            float diff = (prev - pt).magnitude;
            totalLength += diff;
            arr[i] = totalLength;
            prev = pt;
        }
        return arr;
    }

    public static float Sample(float[] fArr, float t)
    {
        int count = fArr.Length;
        if (count == 0)
        {
            Debug.LogError("Unable to sample array - it has no elements");
            return 0;
        }

        if (count == 1)
            return fArr[0];
        float iFloat = t * (float)(count - 1);
        int idLower = Mathf.FloorToInt(iFloat);
        int idUpper = Mathf.FloorToInt(iFloat + 1);
        if (idUpper >= count)
            return fArr[count - 1];
        if (idLower < 0)
            return fArr[0];

        return Mathf.Lerp(fArr[idLower], fArr[idUpper], iFloat - idLower);

    }


    public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);

        float oneMinusT = 1f - t;
        return Mathf.Pow(oneMinusT, 3) * p0 + 3f * Mathf.Pow(oneMinusT, 2) * t * p1 + 3f * oneMinusT * Mathf.Pow(t, 2) * p2 + Mathf.Pow(t, 3) * p3;
    }
    public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return 3f * Mathf.Pow(oneMinusT, 2) * (p1 - p0) + 6f * oneMinusT * t * (p2 - p1) + 3f * Mathf.Pow(t, 2) * (p3 - p2);
    }
}