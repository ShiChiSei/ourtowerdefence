﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Polygon : MonoBehaviour {

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        //transform.TransformPoint(points[

    }

    public Vector3[] Points; //вершины многоугольника 

    private float minX, maxX, minY, maxY;

    public Vector3 Max { get { return new Vector3(maxX, maxY); } }
    public Vector3 Min { get { return new Vector3(minX, minY); } }
    public float Width { get { return maxX - minX; } }
    public float Height { get { return maxY - minY; } }

    public void Polygonf()
    {
        if (Points.Length < 3)
            throw new Exception();

        minX = minY = float.MaxValue;
        maxX = maxY = float.MinValue;

        for (int i = 0; i < Points.Length; i += 2)
        {
            minX = Math.Min(minX, Points[i].x);
            maxX = Math.Max(maxX, Points[i].x);
            minY = Math.Min(minY, Points[i].y);
            maxY = Math.Max(maxY, Points[i].y);
        }
    }

    private PointOverEdge Classify(Vector3 p, Vector3 v, Vector3 w) //положение точки p относительно отрезка vw
    {
        //коэффициенты уравнения прямой
        float a = v.y - w.y;
        float b = w.x - v.x;
        float c = v.x * w.y - w.x * v.y;

        //подставим точку в уравнение прямой
        float f = a * p.x + b * p.y + c;
        if (f > 0)
            return PointOverEdge.RIGHT; //точка лежит справа от отрезка
        if (f < 0)
            return PointOverEdge.LEFT; //слева от отрезка

        float minX = Math.Min(v.x, w.x);
        float maxX = Math.Max(v.x, w.x);
        float minY = Math.Min(v.y, w.y);
        float maxY = Math.Max(v.y, w.y);

        if (minX <= p.x && p.x <= maxX && minY <= p.y && p.y <= maxY)
            return PointOverEdge.BETWEEN; //точка лежит на отрезке
        return PointOverEdge.OUTSIDE; //точка лежит на прямой, но не на отрезке
    }

    private EdgeType GetEdgeType(Vector3 a, Vector3 v, Vector3 w) //тип ребра vw для точки a
    {
        switch (Classify(a, v, w))
        {
            case PointOverEdge.LEFT:
                return ((v.y < a.y) && (a.y <= w.y)) ? EdgeType.CROSSING : EdgeType.INESSENTIAL;
            case PointOverEdge.RIGHT:
                return ((w.y < a.y) && (a.y <= v.y)) ? EdgeType.CROSSING : EdgeType.INESSENTIAL;
            case PointOverEdge.BETWEEN:
                return EdgeType.TOUCHING;
            default:
                return EdgeType.INESSENTIAL;
        }
    }

    private PointInPolygon GetPointInPolygon(Vector3 a) //положение точки в многоугольнике
    {
        bool parity = true;
        for (int i = 0; i < Points.Length; i++)
        {
            Vector3 v = Points[i];
            Vector3 w = Points[(i + 1) % Points.Length];

            switch (GetEdgeType(a, v, w))
            {
                case EdgeType.TOUCHING:
                    return PointInPolygon.BOUNDARY;
                case EdgeType.CROSSING:
                    parity = !parity;
                    break;
            }
        }

        return parity ? PointInPolygon.OUTSIDE : PointInPolygon.INSIDE;
    }

    public bool InPolygon(Vector3 a) //проверка попадания точки в Polygon
    {
        a = transform.InverseTransformPoint(a);
        Polygon.PointInPolygon touch = GetPointInPolygon(a);
        return touch == Polygon.PointInPolygon.INSIDE || touch == Polygon.PointInPolygon.BOUNDARY;
    }

    public enum PointInPolygon { INSIDE, OUTSIDE, BOUNDARY } //положение точки в многоугольнике

    private enum EdgeType { TOUCHING, CROSSING, INESSENTIAL } //положение ребра

    private enum PointOverEdge { LEFT, RIGHT, BETWEEN, OUTSIDE } //положение точки относительно отрезка
}
