﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(Polygon))]
public class PolygonInspector : Editor
{
    private Polygon polygon;
    private Transform handleTransform;
    private Quaternion handleRotation;
    private int selectedIndex = -1;

    private const float handleSize = 0.04f;
    private const float pickSize = 0.06f;


    private void OnSceneGUI()
    {
        polygon = target as Polygon;
        handleTransform = polygon.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ?
            handleTransform.rotation : Quaternion.identity;

        ShowPoints();
    }

    private void ShowPoints()
    {
        Vector3 p0 = ShowPoint(0);
        for (int i = 1; i < polygon.Points.Length; i++)
        {
            Vector3 p1 = ShowPoint(i);

            Handles.color = Color.gray;
            Handles.DrawLine(p0, p1);

            p0 = p1;
        }
        Handles.DrawLine(handleTransform.InverseTransformPoint(polygon.Points[0]), p0);
    }

    private Vector3 ShowPoint(int index)
    {
        Vector3 point = handleTransform.TransformPoint(polygon.Points[index]);
        float size = HandleUtility.GetHandleSize(point);
        if (index == 0)
        {
            size *= 2f;
        }
        Handles.color = Color.red;

        if (Handles.Button(point, handleRotation, size * handleSize, size * pickSize, Handles.DotHandleCap))
        {
            selectedIndex = index;
            Repaint();
        }

        if (selectedIndex == index)
        {
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, handleRotation);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(polygon, "Move Point");

                polygon.Points[index] = handleTransform.InverseTransformPoint(point);

                EditorUtility.SetDirty(polygon);
            }
        }
        return point;
    }
}